var ObjectID = require('mongodb').ObjectID;

module.exports = function (app, db) {
    app.get('/dishes/:id', (req, res) => {
        const id = req.params.id;
        const details = { '_id': new ObjectID(id) };
        db.db().collection('dishes').findOne(details, (err, item) => {
            if (err) {
                res.send({ 'error': 'An error has occurred' });
            } else {
                res.send(item);
            }
        });
    });
    app.get('/dishes', (req, res) => {
        db.db().collection('dishes').find({}).toArray((err, item) => {
            if (err) {
                res.send({ 'error': 'No dishes' });
            } else {
                res.send(item);
            }
        });
        
    });
    app.post('/dishes', (req, res) => {
        if (req.body.name) {
            const dish = {...req.body};
            db.db().collection('dishes').insertOne(dish, (err, result) => {
                if (err) {
                    res.send({ 'error': 'An error has occurred' });
                } else {
                    res.send(result.ops[0]);
                }
            });
        }else{
            res.send({ 'error': 'Empty name' });
        }

    });
    app.delete('/dishes/:id', (req, res) => {
        const id = req.params.id;
        const details = { '_id': new ObjectID(id) };
        db.db().collection('dishes').deleteOne(details, (err, item) => {
            if (err) {
                res.send({ 'error': 'An error has occurred' });
            } else {
                res.send('Note ' + id + ' deleted!');
            }
        });
    });
    app.put('/dishes/:id', (req, res) => {
        const id = req.params.id;
        const details = { '_id': new ObjectID(id) };
        const dish = { ...req.body};
        delete dish._id;
        db.db().collection('dishes').updateOne(details,{$set: dish},{ upsert: true }, (err, result) => {
            if (err) {
                console.log(err);
                
                res.send({ 'error': 'An error has occurred' });
            } else {
                res.send({ 'Ok': 'Updated' });
            }
        });
    });
};